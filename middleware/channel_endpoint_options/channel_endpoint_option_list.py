import json
from database import database
from . import channel_endpoint_option


def load_channel_endpoint_option_list():
    data  = database.db.fetchall('SELECT * FROM channel_endpoint_option_mapping')
    channel_endpoint_option_list = []
    for row in data:
        tmp_channel_endpoint_option = channel_endpoint_option.ChannelEndpointOption()
        tmp_channel_endpoint_option.database_id = row['id']
        tmp_channel_endpoint_option.channel_id = row['channel_id']
        tmp_channel_endpoint_option.endpoint_id = row['endpoint_id']
        tmp_channel_endpoint_option.options = json.loads(row['options'])
        channel_endpoint_option_list.append(tmp_channel_endpoint_option)
    return channel_endpoint_option_list


def get_options_by_channel_and_endpoint(channel_id, endpoint_id):
    for option in CHANNEL_ENDPOINT_OPTION_LIST:
        if option.endpoint_id == endpoint_id and option.channel_id == channel_id:
            return option.options
    return False


CHANNEL_ENDPOINT_OPTION_LIST = load_channel_endpoint_option_list()
