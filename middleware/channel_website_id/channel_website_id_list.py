from database import database
from . import channel_website_id


def load_channel_website_id_list():
    data = database.db.fetchall('SELECT * FROM channel_website_id_mapping')
    channel_website_id_list = []
    for row in data:
        tmp_channel_website_id = channel_website_id.ChannelWebsiteId()
        tmp_channel_website_id.id = row['id']
        tmp_channel_website_id.type = row['type']
        tmp_channel_website_id.subtype = row['subtype']
        tmp_channel_website_id.website_id = row['website_id']
        tmp_channel_website_id.shipping_method_id = row['shipping_method_id']
        tmp_channel_website_id.payment_method_id = row['payment_method_id']
        tmp_channel_website_id.vat_id = row['vat_id']
        channel_website_id_list.append(tmp_channel_website_id)
    return channel_website_id_list


def get_website_id_by_type(type, subtype):
    for cwi in CHANNEL_WEBSITE_ID_LIST:
        if cwi.type == type and cwi.subtype == subtype:
            return cwi


CHANNEL_WEBSITE_ID_LIST = load_channel_website_id_list()
