from quart import Quart
from config import logger_config
from middleware.products.controllers import product
from middleware.orders.controllers import order
from middleware.cron.controllers import cron_controller

app = Quart(__name__)
app.register_blueprint(product, url_prefix='/products')
app.register_blueprint(order, url_prefix='/orders')
app.register_blueprint(cron_controller, url_prefix='/cron')