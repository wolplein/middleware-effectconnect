class ConvertCountry:
    two_letter_code = ""
    three_letter_code = ""
    full_country_name = ""
    need_customs_paper = False

    def __init__(self, country_code):
        self._convert_country_codes(country_code)
        self.full_country_name = self._convert_country_code_fulltext(self.three_letter_code)

    _country_two_three = {
        "AFG": "AF",  # Afghanistan
        "ALB": "AL",  # Albania
        "DZA": "DZ",  # Algeria
        "ASM": "AS",  # American Samoa
        "AND": "AD",  # Andorra
        "AGO": "AO",  # Angola
        "AIA": "AI",  # Anguilla
        "ATA": "AQ",  # Antarctica
        "ATG": "AG",  # Antigua and Barbuda
        "ARG": "AR",  # Argentina
        "ARM": "AM",  # Armenia
        "ABW": "AW",  # Aruba
        "AUS": "AU",  # Australia
        "AUT": "AT",  # Austria
        "AZE": "AZ",  # Azerbaijan
        "BHS": "BS",  # Bahamas (the)
        "BHR": "BH",  # Bahrain
        "BGD": "BD",  # Bangladesh
        "BRB": "BB",  # Barbados
        "BLR": "BY",  # Belarus
        "BEL": "BE",  # Belgium
        "BLZ": "BZ",  # Belize
        "BEN": "BJ",  # Benin
        "BMU": "BM",  # Bermuda
        "BTN": "BT",  # Bhutan
        "BOL": "BO",  # Bolivia (Plurinational State of)
        "BES": "BQ",  # Bonaire, Sint Eustatius and Saba
        "BIH": "BA",  # Bosnia and Herzegovina
        "BWA": "BW",  # Botswana
        "BVT": "BV",  # Bouvet Island
        "BRA": "BR",  # Brazil
        "IOT": "IO",  # British Indian Ocean Territory (the)
        "BRN": "BN",  # Brunei Darussalam
        "BGR": "BG",  # Bulgaria
        "BFA": "BF",  # Burkina Faso
        "BDI": "BI",  # Burundi
        "CPV": "CV",  # Cabo Verde
        "KHM": "KH",  # Cambodia
        "CMR": "CM",  # Cameroon
        "CAN": "CA",  # Canada
        "CYM": "KY",  # Cayman Islands (the)
        "CAF": "CF",  # Central African Republic (the)
        "TCD": "TD",  # Chad
        "CHL": "CL",  # Chile
        "CHN": "CN",  # China
        "CXR": "CX",  # Christmas Island
        "CCK": "CC",  # Cocos (Keeling) Islands (the)
        "COL": "CO",  # Colombia
        "COM": "KM",  # Comoros (the)
        "COD": "CD",  # Congo (the Democratic Republic of the)
        "COG": "CG",  # Congo (the)
        "COK": "CK",  # Cook Islands (the)
        "CRI": "CR",  # Costa Rica
        "HRV": "HR",  # Croatia
        "CUB": "CU",  # Cuba
        "CUW": "CW",  # Curaçao
        "CYP": "CY",  # Cyprus
        "CZE": "CZ",  # Czechia
        "CIV": "CI",  # Côte d'Ivoire
        "DNK": "DK",  # Denmark
        "DJI": "DJ",  # Djibouti
        "DMA": "DM",  # Dominica
        "DOM": "DO",  # Dominican Republic (the)
        "ECU": "EC",  # Ecuador
        "EGY": "EG",  # Egypt
        "SLV": "SV",  # El Salvador
        "GNQ": "GQ",  # Equatorial Guinea
        "ERI": "ER",  # Eritrea
        "EST": "EE",  # Estonia
        "SWZ": "SZ",  # Eswatini
        "ETH": "ET",  # Ethiopia
        "FLK": "FK",  # Falkland Islands (the) [Malvinas]
        "FRO": "FO",  # Faroe Islands (the)
        "FJI": "FJ",  # Fiji
        "FIN": "FI",  # Finland
        "FRA": "FR",  # France
        "GUF": "GF",  # French Guiana
        "PYF": "PF",  # French Polynesia
        "ATF": "TF",  # French Southern Territories (the)
        "GAB": "GA",  # Gabon
        "GMB": "GM",  # Gambia (the)
        "GEO": "GE",  # Georgia
        "DEU": "DE",  # Germany
        "GHA": "GH",  # Ghana
        "GIB": "GI",  # Gibraltar
        "GRC": "GR",  # Greece
        "GRL": "GL",  # Greenland
        "GRD": "GD",  # Grenada
        "GLP": "GP",  # Guadeloupe
        "GUM": "GU",  # Guam
        "GTM": "GT",  # Guatemala
        "GGY": "GG",  # Guernsey
        "GIN": "GN",  # Guinea
        "GNB": "GW",  # Guinea-Bissau
        "GUY": "GY",  # Guyana
        "HTI": "HT",  # Haiti
        "HMD": "HM",  # Heard Island and McDonald Islands
        "VAT": "VA",  # Holy See (the)
        "HND": "HN",  # Honduras
        "HKG": "HK",  # Hong Kong
        "HUN": "HU",  # Hungary
        "ISL": "IS",  # Iceland
        "IND": "IN",  # India
        "IDN": "ID",  # Indonesia
        "IRN": "IR",  # Iran (Islamic Republic of)
        "IRQ": "IQ",  # Iraq
        "IRL": "IE",  # Ireland
        "IMN": "IM",  # Isle of Man
        "ISR": "IL",  # Israel
        "ITA": "IT",  # Italy
        "JAM": "JM",  # Jamaica
        "JPN": "JP",  # Japan
        "JEY": "JE",  # Jersey
        "JOR": "JO",  # Jordan
        "KAZ": "KZ",  # Kazakhstan
        "KEN": "KE",  # Kenya
        "KIR": "KI",  # Kiribati
        "PRK": "KP",  # Korea (the Democratic People's Republic of)
        "KOR": "KR",  # Korea (the Republic of)
        "KWT": "KW",  # Kuwait
        "KGZ": "KG",  # Kyrgyzstan
        "LAO": "LA",  # Lao People's Democratic Republic (the)
        "LVA": "LV",  # Latvia
        "LBN": "LB",  # Lebanon
        "LSO": "LS",  # Lesotho
        "LBR": "LR",  # Liberia
        "LBY": "LY",  # Libya
        "LIE": "LI",  # Liechtenstein
        "LTU": "LT",  # Lithuania
        "LUX": "LU",  # Luxembourg
        "MAC": "MO",  # Macao
        "MKD": "MK",  # Macedonia (the former Yugoslav Republic of)
        "MDG": "MG",  # Madagascar
        "MWI": "MW",  # Malawi
        "MYS": "MY",  # Malaysia
        "MDV": "MV",  # Maldives
        "MLI": "ML",  # Mali
        "MLT": "MT",  # Malta
        "MHL": "MH",  # Marshall Islands (the)
        "MTQ": "MQ",  # Martinique
        "MRT": "MR",  # Mauritania
        "MUS": "MU",  # Mauritius
        "MYT": "YT",  # Mayotte
        "MEX": "MX",  # Mexico
        "FSM": "FM",  # Micronesia (Federated States of)
        "MDA": "MD",  # Moldova (the Republic of)
        "MCO": "MC",  # Monaco
        "MNG": "MN",  # Mongolia
        "MNE": "ME",  # Montenegro
        "MSR": "MS",  # Montserrat
        "MAR": "MA",  # Morocco
        "MOZ": "MZ",  # Mozambique
        "MMR": "MM",  # Myanmar
        "NAM": "NA",  # Namibia
        "NRU": "NR",  # Nauru
        "NPL": "NP",  # Nepal
        "NLD": "NL",  # Netherlands (the)
        "NCL": "NC",  # New Caledonia
        "NZL": "NZ",  # New Zealand
        "NIC": "NI",  # Nicaragua
        "NER": "NE",  # Niger (the)
        "NGA": "NG",  # Nigeria
        "NIU": "NU",  # Niue
        "NFK": "NF",  # Norfolk Island
        "MNP": "MP",  # Northern Mariana Islands (the)
        "NOR": "NO",  # Norway
        "OMN": "OM",  # Oman
        "PAK": "PK",  # Pakistan
        "PLW": "PW",  # Palau
        "PSE": "PS",  # Palestine, State of
        "PAN": "PA",  # Panama
        "PNG": "PG",  # Papua New Guinea
        "PRY": "PY",  # Paraguay
        "PER": "PE",  # Peru
        "PHL": "PH",  # Philippines (the)
        "PCN": "PN",  # Pitcairn
        "POL": "PL",  # Poland
        "PRT": "PT",  # Portugal
        "PRI": "PR",  # Puerto Rico
        "QAT": "QA",  # Qatar
        "ROU": "RO",  # Romania
        "RUS": "RU",  # Russian Federation (the)
        "RWA": "RW",  # Rwanda
        "REU": "RE",  # Réunion
        "BLM": "BL",  # Saint Barthélemy
        "SHN": "SH",  # Saint Helena, Ascension and Tristan da Cunha
        "KNA": "KN",  # Saint Kitts and Nevis
        "LCA": "LC",  # Saint Lucia
        "MAF": "MF",  # Saint Martin (French part)
        "SPM": "PM",  # Saint Pierre and Miquelon
        "VCT": "VC",  # Saint Vincent and the Grenadines
        "WSM": "WS",  # Samoa
        "SMR": "SM",  # San Marino
        "STP": "ST",  # Sao Tome and Principe
        "SAU": "SA",  # Saudi Arabia
        "SEN": "SN",  # Senegal
        "SRB": "RS",  # Serbia
        "SYC": "SC",  # Seychelles
        "SLE": "SL",  # Sierra Leone
        "SGP": "SG",  # Singapore
        "SXM": "SX",  # Sint Maarten (Dutch part)
        "SVK": "SK",  # Slovakia
        "SVN": "SI",  # Slovenia
        "SLB": "SB",  # Solomon Islands
        "SOM": "SO",  # Somalia
        "ZAF": "ZA",  # South Africa
        "SGS": "GS",  # South Georgia and the South Sandwich Islands
        "SSD": "SS",  # South Sudan
        "ESP": "ES",  # Spain
        "LKA": "LK",  # Sri Lanka
        "SDN": "SD",  # Sudan (the)
        "SUR": "SR",  # Suriname
        "SJM": "SJ",  # Svalbard and Jan Mayen
        "SWE": "SE",  # Sweden
        "CHE": "CH",  # Switzerland
        "SYR": "SY",  # Syrian Arab Republic
        "TWN": "TW",  # Taiwan (Province of China)
        "TJK": "TJ",  # Tajikistan
        "TZA": "TZ",  # Tanzania, United Republic of
        "THA": "TH",  # Thailand
        "TLS": "TL",  # Timor-Leste
        "TGO": "TG",  # Togo
        "TKL": "TK",  # Tokelau
        "TON": "TO",  # Tonga
        "TTO": "TT",  # Trinidad and Tobago
        "TUN": "TN",  # Tunisia
        "TUR": "TR",  # Turkey
        "TKM": "TM",  # Turkmenistan
        "TCA": "TC",  # Turks and Caicos Islands (the)
        "TUV": "TV",  # Tuvalu
        "UGA": "UG",  # Uganda
        "UKR": "UA",  # Ukraine
        "ARE": "AE",  # United Arab Emirates (the)
        "GBR": "GB",  # United Kingdom of Great Britain and Northern Ireland (the)
        "UMI": "UM",  # United States Minor Outlying Islands (the)
        "USA": "US",  # United States of America (the)
        "URY": "UY",  # Uruguay
        "UZB": "UZ",  # Uzbekistan
        "VUT": "VU",  # Vanuatu
        "VEN": "VE",  # Venezuela (Bolivarian Republic of)
        "VNM": "VN",  # Viet Nam
        "VGB": "VG",  # Virgin Islands (British)
        "VIR": "VI",  # Virgin Islands (U.S.)
        "WLF": "WF",  # Wallis and Futuna
        "ESH": "EH",  # Western Sahara
        "YEM": "YE",  # Yemen
        "ZMB": "ZM",  # Zambia
        "ZWE": "ZW",  # Zimbabwe
        "ALA": "AX",  # Åland Islands
    }

    _full_country_mapping = {
        "AFG": "Afghanistan",
        "ALA": "Åland Islands",
        "ALB": "Albania",
        "DZA": "Algeria",
        "ASM": "American Samoa",
        "AND": "Andorra",
        "AGO": "Angola",
        "AIA": "Anguilla",
        "ATA": "Antarctica",
        "ATG": "Antigua and Barbuda",
        "ARG": "Argentina",
        "ARM": "Armenia",
        "ABW": "Aruba",
        "AUS": "Australia",
        "AUT": "Austria",
        "AZE": "Azerbaijan",
        "BHS": "Bahamas (the)",
        "BHR": "Bahrain",
        "BGD": "Bangladesh",
        "BRB": "Barbados",
        "BLR": "Belarus",
        "BEL": "Belgium",
        "BLZ": "Belize",
        "BEN": "Benin",
        "BMU": "Bermuda",
        "BTN": "Bhutan",
        "BOL": "Bolivia (Plurinational State of)",
        "BES": "Bonaire, Sint Eustatius and Saba",
        "BIH": "Bosnia and Herzegovina",
        "BWA": "Botswana",
        "BVT": "Bouvet Island",
        "BRA": "Brazil",
        "IOT": "British Indian Ocean Territory (the)",
        "BRN": "Brunei Darussalam",
        "BGR": "Bulgaria",
        "BFA": "Burkina Faso",
        "BDI": "Burundi",
        "CPV": "Cabo Verde",
        "KHM": "Cambodia",
        "CMR": "Cameroon",
        "CAN": "Canada",
        "CYM": "Cayman Islands (the)",
        "CAF": "Central African Republic (the)",
        "TCD": "Chad",
        "CHL": "Chile",
        "CHN": "China",
        "CXR": "Christmas Island",
        "CCK": "Cocos (Keeling) Islands (the)",
        "COL": "Colombia",
        "COM": "Comoros (the)",
        "COD": "Congo (the Democratic Republic of the)",
        "COG": "Congo (the)",
        "COK": "Cook Islands (the)",
        "CRI": "Costa Rica",
        "CIV": "Côte d'Ivoire",
        "HRV": "Croatia",
        "CUB": "Cuba",
        "CUW": "Curaçao",
        "CYP": "Cyprus",
        "CZE": "Czechia",
        "DNK": "Denmark",
        "DJI": "Djibouti",
        "DMA": "Dominica",
        "DOM": "Dominican Republic (the)",
        "ECU": "Ecuador",
        "EGY": "Egypt",
        "SLV": "El Salvador",
        "GNQ": "Equatorial Guinea",
        "ERI": "Eritrea",
        "EST": "Estonia",
        "SWZ": "Eswatini",
        "ETH": "Ethiopia",
        "FLK": "Falkland Islands (the) [Malvinas]",
        "FRO": "Faroe Islands (the)",
        "FJI": "Fiji",
        "FIN": "Finland",
        "FRA": "France",
        "GUF": "French Guiana",
        "PYF": "French Polynesia",
        "ATF": "French Southern Territories (the)",
        "GAB": "Gabon",
        "GMB": "Gambia (the)",
        "GEO": "Georgia",
        "DEU": "Germany",
        "GHA": "Ghana",
        "GIB": "Gibraltar",
        "GRC": "Greece",
        "GRL": "Greenland",
        "GRD": "Grenada",
        "GLP": "Guadeloupe",
        "GUM": "Guam",
        "GTM": "Guatemala",
        "GGY": "Guernsey",
        "GIN": "Guinea",
        "GNB": "Guinea-Bissau",
        "GUY": "Guyana",
        "HTI": "Haiti",
        "HMD": "Heard Island and McDonald Islands",
        "VAT": "Holy See (the)",
        "HND": "Honduras",
        "HKG": "Hong Kong",
        "HUN": "Hungary",
        "ISL": "Iceland",
        "IND": "India",
        "IDN": "Indonesia",
        "IRN": "Iran (Islamic Republic of)",
        "IRQ": "Iraq",
        "IRL": "Ireland",
        "IMN": "Isle of Man",
        "ISR": "Israel",
        "ITA": "Italy",
        "JAM": "Jamaica",
        "JPN": "Japan",
        "JEY": "Jersey",
        "JOR": "Jordan",
        "KAZ": "Kazakhstan",
        "KEN": "Kenya",
        "KIR": "Kiribati",
        "PRK": "Korea (the Democratic People's Republic of)",
        "KOR": "Korea (the Republic of)",
        "KWT": "Kuwait",
        "KGZ": "Kyrgyzstan",
        "LAO": "Lao People's Democratic Republic (the)",
        "LVA": "Latvia",
        "LBN": "Lebanon",
        "LSO": "Lesotho",
        "LBR": "Liberia",
        "LBY": "Libya",
        "LIE": "Liechtenstein",
        "LTU": "Lithuania",
        "LUX": "Luxembourg",
        "MAC": "Macao",
        "MKD": "Macedonia (the former Yugoslav Republic of)",
        "MDG": "Madagascar",
        "MWI": "Malawi",
        "MYS": "Malaysia",
        "MDV": "Maldives",
        "MLI": "Mali",
        "MLT": "Malta",
        "MHL": "Marshall Islands (the)",
        "MTQ": "Martinique",
        "MRT": "Mauritania",
        "MUS": "Mauritius",
        "MYT": "Mayotte",
        "MEX": "Mexico",
        "FSM": "Micronesia (Federated States of)",
        "MDA": "Moldova (the Republic of)",
        "MCO": "Monaco",
        "MNG": "Mongolia",
        "MNE": "Montenegro",
        "MSR": "Montserrat",
        "MAR": "Morocco",
        "MOZ": "Mozambique",
        "MMR": "Myanmar",
        "NAM": "Namibia",
        "NRU": "Nauru",
        "NPL": "Nepal",
        "NLD": "Netherlands (the)",
        "NCL": "New Caledonia",
        "NZL": "New Zealand",
        "NIC": "Nicaragua",
        "NER": "Niger (the)",
        "NGA": "Nigeria",
        "NIU": "Niue",
        "NFK": "Norfolk Island",
        "MNP": "Northern Mariana Islands (the)",
        "NOR": "Norway",
        "OMN": "Oman",
        "PAK": "Pakistan",
        "PLW": "Palau",
        "PSE": "Palestine, State of",
        "PAN": "Panama",
        "PNG": "Papua New Guinea",
        "PRY": "Paraguay",
        "PER": "Peru",
        "PHL": "Philippines (the)",
        "PCN": "Pitcairn",
        "POL": "Poland",
        "PRT": "Portugal",
        "PRI": "Puerto Rico",
        "QAT": "Qatar",
        "REU": "Réunion",
        "ROU": "Romania",
        "RUS": "Russian Federation (the)",
        "RWA": "Rwanda",
        "BLM": "Saint Barthélemy",
        "SHN": "Saint Helena, Ascension and Tristan da Cunha",
        "KNA": "Saint Kitts and Nevis",
        "LCA": "Saint Lucia",
        "MAF": "Saint Martin (French part)",
        "SPM": "Saint Pierre and Miquelon",
        "VCT": "Saint Vincent and the Grenadines",
        "WSM": "Samoa",
        "SMR": "San Marino",
        "STP": "Sao Tome and Principe",
        "SAU": "Saudi Arabia",
        "SEN": "Senegal",
        "SRB": "Serbia",
        "SYC": "Seychelles",
        "SLE": "Sierra Leone",
        "SGP": "Singapore",
        "SXM": "Sint Maarten (Dutch part)",
        "SVK": "Slovakia",
        "SVN": "Slovenia",
        "SLB": "Solomon Islands",
        "SOM": "Somalia",
        "ZAF": "South Africa",
        "SGS": "South Georgia and the South Sandwich Islands",
        "SSD": "South Sudan",
        "ESP": "Spain",
        "LKA": "Sri Lanka",
        "SDN": "Sudan (the)",
        "SUR": "Suriname",
        "SJM": "Svalbard and Jan Mayen",
        "SWE": "Sweden",
        "CHE": "Switzerland",
        "SYR": "Syrian Arab Republic",
        "TWN": "Taiwan (Province of China)",
        "TJK": "Tajikistan",
        "TZA": "Tanzania, United Republic of",
        "THA": "Thailand",
        "TLS": "Timor-Leste",
        "TGO": "Togo",
        "TKL": "Tokelau",
        "TON": "Tonga",
        "TTO": "Trinidad and Tobago",
        "TUN": "Tunisia",
        "TUR": "Turkey",
        "TKM": "Turkmenistan",
        "TCA": "Turks and Caicos Islands (the)",
        "TUV": "Tuvalu",
        "UGA": "Uganda",
        "UKR": "Ukraine",
        "ARE": "United Arab Emirates (the)",
        "GBR": "United Kingdom of Great Britain and Northern Ireland (the)",
        "UMI": "United States Minor Outlying Islands (the)",
        "USA": "United States of America (the)",
        "URY": "Uruguay",
        "UZB": "Uzbekistan",
        "VUT": "Vanuatu",
        "VEN": "Venezuela (Bolivarian Republic of)",
        "VNM": "Viet Nam",
        "VGB": "Virgin Islands (British)",
        "VIR": "Virgin Islands (U.S.)",
        "WLF": "Wallis and Futuna",
        "ESH": "Western Sahara",
        "YEM": "Yemen",
        "ZMB": "Zambia",
        "ZWE": "Zimbabwe",
    }

    def _convert_country_codes(self, country_code):
        country_code = str(country_code)
        # Convert from three-letter code
        if len(country_code) == 3:
            country_code = country_code.upper()
            if country_code in self._country_two_three:
                self.two_letter_code = self._country_two_three[country_code]
        # Convert from two-letter code
        elif len(country_code) == 2:
            country_code = country_code.upper()
            for three_code, two_code in self._country_two_three.items():
                if two_code == country_code:
                    self.two_letter_code = country_code
                    self.three_letter_code = three_code
                    break
        # Convert from fulltext
        else:
            country_code = country_code.title()
            for three_code, full_name in self._full_country_mapping.items():
                if full_name == country_code:
                    self.three_letter_code = three_code
                    self.two_letter_code = self._country_two_three[three_code]

    def _convert_country_code_fulltext(self, country_code):
        if len(str(country_code)) == 3:
            if country_code in self._full_country_mapping:
                return self._full_country_mapping[country_code]
        return ""

