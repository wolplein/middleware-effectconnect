import re
from database import database


class Customer:
    def __init__(self):
        self.customer_id = 0
        self.order_id = 0
        self.type = ""
        self.company_name = ""
        self.firstname = ""
        self.lastname = ""
        self.street = ""
        self.street2 = ""
        self.housenumber = ""
        self.housenumber_addition = ""
        self.zipcode = ""
        self.city = ""
        self.state_province = ""
        self.dynamic_country = False
        self.country_code = ""
        self.email = ""
        self.telephone = ""
        self.separate_house_number_country = [
            "AUT",  # Austria
            "BEL",  # Belgium
            "BGR",  # Bulgaria
            "CYP",  # Cyprus
            "CZE",  # Czechia
            "DNK",  # Denmark
            "DEU",  # Germany
            "EST",  # Estonia
            "FIN",  # Finland
            "HUN",  # Hungary
            "ITA",  # Italy
            "HRV",  # Croatia
            "LVA",  # Latvia
            "LTU",  # Lithuania
            "NLD",  # Netherlands (the)
            "POL",  # Poland
            "PRT",  # Portugal
            "ROU",  # Romania
            "SVN",  # Slovenia
            "SVK",  # Slovakia
            "ESP",  # Spain
            "SWE"   # Sweden
        ]

    def insert_customer(self):
        insert_query = """
                    INSERT INTO customers(
                        order_id, type, company_name, firstname, lastname, street, street2, housenumber,
                        housenumber_addition, zipcode, city, state_province, country_code, email, telephone
                    )
                    VALUES (
                        {order_id}, '{type}', '{company_name}', '{firstname}', '{lastname}', '{street}', '{street2}',
                        '{housenumber}', '{housenumber_addition}', '{zipcode}', '{city}', '{state_province}', 
                        '{country_code}', '{email}', '{telephone}'
                    )"""
        insert_query = insert_query.format(order_id=self.order_id,
                                           type=self.type,
                                           company_name=database.db.db_connection.escape_string(self.company_name),
                                           firstname=database.db.db_connection.escape_string(self.firstname),
                                           lastname=database.db.db_connection.escape_string(self.lastname),
                                           street=database.db.db_connection.escape_string(self.street),
                                           street2=database.db.db_connection.escape_string(self.street2),
                                           housenumber=database.db.db_connection.escape_string(self.housenumber),
                                           housenumber_addition=database.db.db_connection.escape_string(
                                               self.housenumber_addition),
                                           zipcode=database.db.db_connection.escape_string(self.zipcode),
                                           city=database.db.db_connection.escape_string(self.city),
                                           state_province=database.db.db_connection.escape_string(self.state_province),
                                           country_code=database.db.db_connection.escape_string(self.country_code),
                                           email=database.db.db_connection.escape_string(self.email),
                                           telephone=database.db.db_connection.escape_string(self.telephone)
                                           )
        self.customer_id = database.db.no_select_query(insert_query, return_id=True)
        return True

    def load_from_db(self):
        if self.customer_id:
            data = database.db.fetchall("SELECT * FROM customers WHERE customer_id = " + str(self.customer_id))
            customer_data = data[0]
            self.order_id = customer_data['order_id']
            self.type = customer_data['type']
            self.company_name = customer_data['company_name']
            self.firstname = customer_data['firstname']
            self.lastname = customer_data['lastname']
            self.street = customer_data['street']
            self.street2 = customer_data['street2']
            self.housenumber = customer_data['housenumber']
            self.housenumber_addition = customer_data['housenumber_addition']
            self.zipcode = customer_data['zipcode']
            self.city = customer_data['city']
            self.state_province = customer_data['state_province']
            self.country_code = customer_data['country_code']
            self.email = customer_data['email']
            self.telephone = customer_data['telephone']

    @staticmethod
    def split_fullname(fullname):
        firstname = fullname.strip().split(' ')[0]
        lastname = ' '.join((fullname + ' ').split(' ')[1:]).strip()
        return firstname, lastname

    def get_full_street(self):
        return (self.street + " " + self.housenumber + " " + self.housenumber_addition).rstrip()

    @staticmethod
    def unique_shipping_customer(order_id):
        data = database.db.fetchall("SELECT * FROM customers WHERE order_id = " + str(order_id))
        for customer_row in data:
            if customer_row['type'] == 'SHIPPING':
                shipping_customer = customer_row
                del shipping_customer['type'], shipping_customer['customer_id']
            elif customer_row['type'] == 'BILLING':
                billing_customer = customer_row
                del billing_customer['type'], billing_customer['customer_id']
        if billing_customer == shipping_customer:
            return False
        else:
            return True

    def split_housenumber(self, street1):
        result = re.search(r"^(\d*[\wäöüß\d '\-\.]+)[,\s]+(\d+)\s*([\wäöüß\d\-\/]*)$", street1.strip())
        if result:
            self.street = result.group(1)
            self.housenumber = result.group(2)
            if result.group(3):
                self.housenumber_addition = result.group(3)