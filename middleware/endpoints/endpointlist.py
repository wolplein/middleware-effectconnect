import json
import importlib
from database import database
from .endpoint import Endpoint


def load_endpoint_list():
    data = database.db.fetchall("SELECT * FROM endpoints where active = 1")
    endpoint_list = []
    for row in data:
        import_type = row["import_type"]
        endpoint_module = importlib.import_module(
            'middleware.endpoints.' + import_type + '.' + import_type + '_endpoint')

        tmp_endpoint = getattr(endpoint_module, endpoint_dict[import_type] + 'Endpoint')()
        if not tmp_endpoint:
            tmp_endpoint = Endpoint()

        tmp_endpoint.database = row['database']
        tmp_endpoint.public_key = row['public_key']
        tmp_endpoint.private_key = row['private_key']
        tmp_endpoint.company_key = row['company_key']
        tmp_endpoint.endpoint_id = row['endpoint_id']
        tmp_endpoint.endpoint_url = row['endpoint_url']
        tmp_endpoint.import_type = import_type
        tmp_endpoint.user_id = row['user_id']
        tmp_endpoint.username = row['username']
        tmp_endpoint.password = row['password']
        tmp_endpoint.endpoint_options = json.loads(row['endpoint_options'])

        client_module = importlib.import_module('middleware.endpoints.' + import_type + '.' + import_type + '_client')
        tmp_endpoint.client = getattr(client_module, endpoint_dict[import_type] + 'Client')(tmp_endpoint)
        endpoint_list.append(tmp_endpoint)
    return endpoint_list


def get_endpoint_by_id(endpoint_id):
    for endpoint in ENDPOINT_LIST:
        if endpoint.endpoint_id == endpoint_id:
            return endpoint
    raise Exception("No endpoint found for id " + str(endpoint_id))


endpoint_dict = {
    'logic4': 'Logic4'
}

ENDPOINT_LIST = load_endpoint_list()
