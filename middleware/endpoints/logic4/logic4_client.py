import base64
import datetime
import hmac
import hashlib
import json
import requests
import uuid


class Logic4Client:
    url = False
    public_key = False
    private_key = False
    company_key = False
    administration = False
    user_id = False
    username = False
    password = False
    bearer = ""

    def __init__(self, endpoint):
        self.url = endpoint.endpoint_url
        self.public_key = endpoint.public_key
        self.private_key = endpoint.private_key
        self.company_key = endpoint.company_key
        self.administration = endpoint.database
        self.user_id = endpoint.user_id
        self.username = endpoint.username
        self.password = endpoint.password
        self.set_bearer()

    def set_bearer(self):
        user_dict = {
            "UserName": self.username,
            "Password": self.password
        }
        bearer_result = self.post("User/GetUserBearerToken", user_dict)
        self.bearer = bearer_result['Value']

    def calc_auth_token(self, request_method):
        timestamp = str(int(datetime.datetime.now().timestamp()))
        unique = uuid.uuid4()
        timestamp += str(unique.int)[:5]
        secret_key_byte_array = base64.b64decode(self.private_key.encode('utf-8'))
        nonce = base64.b64encode((self.private_key + timestamp).encode('utf-8')).decode('utf-8')
        hashed_string = self.public_key + self.company_key + request_method + timestamp + nonce
        hmac_string = hmac.new(secret_key_byte_array, msg=hashed_string.encode('utf-8'), digestmod=hashlib.sha256)
        hash_string = base64.b64encode(hmac_string.digest()).decode('utf-8')
        auth_token = self.public_key + ':' + self.company_key + ':' + hash_string + ':' + nonce + ':' + timestamp + ':' + self.administration + ':' + self.user_id
        return auth_token

    def get(self, endpoint):
        return json.loads(requests.get(self.url + endpoint,
                                       headers={'X-LOGIC4-Authorization': self.calc_auth_token('GET')}
                                       ).content
                          )

    def post(self, endpoint, postdict):
        test = json.dumps(postdict)
        post = requests.post(self.url + endpoint,
                             headers={'X-LOGIC4-Authorization': self.calc_auth_token('POST'),
                                      'x-logic4-userbearer': self.bearer,
                                      'Content-Type': 'application/json'},
                             data=json.dumps(postdict)
                             ).content
        try:
            return_value = json.loads(post)
        except:
            return_value = {"response": post}
        return return_value

