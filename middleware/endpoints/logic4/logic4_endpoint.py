from middleware.endpoints.endpoint import Endpoint
from .models.product_category import Logic4ProductCategories
from .models.product import Logic4ListOfProducts
from .models.product_channel_details import Logic4ListOfProductChannelDetails
from .models.order import Logic4Order


class Logic4Endpoint(Endpoint):
    def get_product_categories(self):
        return Logic4ProductCategories(self)

    def get_list_of_products(self):
        return Logic4ListOfProducts(self)

    def get_list_of_product_channel_details(self):
        return Logic4ListOfProductChannelDetails(self)

    def get_order(self):
        return Logic4Order(self)