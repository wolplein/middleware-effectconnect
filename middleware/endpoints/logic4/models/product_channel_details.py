from .product import Logic4Product
from middleware.products.models.product_channel_details import ListOfProductChannelDetails


class Logic4ListOfProductChannelDetails(ListOfProductChannelDetails):
    def __init__(self, endpoint):
        super().__init__()
        self.endpoint = endpoint

    def get_list_per_channel(self):
        super().get_list_per_channel()
        for row in self.list_of_pcds:
            row.product = Logic4Product(self.endpoint)
            row.product.product_id = row.product_id
            row.product.load_from_db()

    def import_prices(self, pricelist_id):
        pricelist_filter = {
            "PricelistId": pricelist_id,
            "LoadContractPrices": True
        }
        pricelist_response = self.endpoint.client.post('Products/GetPricelists', pricelist_filter)
        if 'Records' in pricelist_response:
            prices = pricelist_response['Records'][0]['ContractPrices']
            for product_channel_details in self.list_of_pcds:
                price_dict = next((price for price in prices if price['ProductId'] == product_channel_details.product.endpoint_product_id), None)
                if price_dict:
                    if product_channel_details.price != price_dict['PriceEx']:
                        product_channel_details.price = price_dict['PriceEx']
                        product_channel_details.update_price()
                else:
                    logic4_products = self.endpoint.client.post('Products/GetProducts', {
                        'ProductIds': [product_channel_details.product.endpoint_product_id],
                        'UseECommerceProductGroups': True})
                    if 'Records' in logic4_products:
                        for logic4_product in logic4_products['Records']:
                            product_channel_details.price = round(
                                logic4_product["SellPriceGross"] * (1 + (logic4_product['VatPercent'] / 100)), 2)
                            product_channel_details.update_price()
