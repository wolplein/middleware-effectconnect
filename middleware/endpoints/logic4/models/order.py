from datetime import datetime
from middleware.orders.models.order import Order
from middleware.customers.models.customer import Customer
from middleware.customers.models.convert_country import ConvertCountry
from middleware.channel_website_id import channel_website_id_list


class Logic4Order(Order):
    def __init__(self, endpoint):
        super().__init__()
        self.endpoint = endpoint

    # TODO:  BTW tarief per kanaal
    async def export_order(self):
        try:
            order_data = {
                'CheckForOrderCostAndPaymentRegulation': True,
                'BranchId': self.endpoint.endpoint_options['branch_id'],
                'UserId': int(self.endpoint.user_id),
                'CreationDate': datetime.now().isoformat(),
                'OrderStatus': {
                    'Id': self.endpoint.endpoint_options['order_status_id'],
                },
                'Reference': self.channel_order_number,
                'Description': 'Order via middleware {0}'.format(datetime.now().strftime("%d-%m-%Y %H:%M:%S")),
            }

            channel_website_id = channel_website_id_list.get_website_id_by_type(self.type, self.subtype)

            order_data['ShippingMethod'] = {'Id': channel_website_id.shipping_method_id}
            order_data['PaymentMethod'] = {'Id': channel_website_id.payment_method_id}
            order_data['WebsiteDomainId'] = channel_website_id.website_id

            order_data['DebtorId'], delivery_address = await self.logic4_debtor(channel_website_id.vat_id)
            if delivery_address:
                order_data['DeliveryAddress'] = delivery_address
            order_rows = []
            for row in self.order_items:
                new_row = {
                    'ProductCode': row.sku,
                    'Description': row.product_description,
                    'Qty': float(row.quantity),
                    'WarehouseId': self.endpoint.endpoint_options['warehouse_id'],
                    'InclPrice': row.price
                }
                product = self.endpoint.client.post('Products/GetProducts', {'ProductCode': row.sku})
                if 'Records' in product:
                    if product['Records'][0]['IsAssemblyProduct'] or product['Records'][0]['IsComposedProduct']:
                        new_row['OrderRowWithProductComposition'] = {
                            "AddProductCompositionByParentProductToOrder": True,
                            "UseSystemPricesForProductCompositionProducts": False
                        }
                order_rows.append(new_row)
            order_data['OrderRows'] = order_rows
            response = self.endpoint.client.post('Orders/AddUpdateOrder', order_data)
            if 'Value' in response:
                self.endpoint_order_number = str(response['Value'])
                self.state = 'EXPORTED_ENDPOINT'
            else:
                self.state = 'ERROR_LOGIC4_RESPONSE'
            self.update_order_endpoint_order_number()
        except:
            self.set_order_state('ERROR')

    async def logic4_address(self, address):
        adress_dict = {
            'CompanyName': address.company_name,
            'ContactName': address.firstname + ' ' + address.lastname,
            'Street': address.street,
            'HouseNumber': address.housenumber,
            'HouseNumberAddition': address.housenumber_addition,
            'Address2': address.street2,
            'Zipcode': address.zipcode,
            'City': address.city,
            'CountryCode': address.country_code,
            'Email': address.email,
            'TelephoneNumber': address.telephone,
            'Type': {'Id': 12}
        }
        # if adress.state_province:
        #     adress_dict['Province'] = self.get_logic4_province(adress.country_code, adress.state_province)
        return adress_dict

    async def logic4_debtor(self, vat_id):
        customer_create = {
            'CompanyName': self.billing_customer.company_name,
            'FirstName': self.billing_customer.firstname,
            'LastName': self.billing_customer.lastname,
            'Street': self.billing_customer.street,
            'HouseNumber': self.billing_customer.housenumber,
            'HouseNumberAddition': self.billing_customer.housenumber_addition,
            'Address2': self.billing_customer.street2,
            'Zipcode': self.billing_customer.zipcode,
            'City': self.billing_customer.city,
            'CountryCode': self.billing_customer.country_code,
            'EmailAddress': self.billing_customer.email,
            'TelephoneNumber': self.billing_customer.telephone,
            'VatCode': {'Id': vat_id}
        }
        if self.billing_customer.state_province:
            customer_create['Province'] = self.get_logic4_province(self.billing_customer.country_code,
                                                                   self.billing_customer.state_province)
        customer_response = self.endpoint.client.post('Relations/AddUpdateCustomer', customer_create)
        if Customer.unique_shipping_customer(self.order_id):
            deliver_address = await self.logic4_address(self.shipping_customer)
            return customer_response['Value'], deliver_address
        return customer_response['Value'], False

    def get_logic4_province(self, country_code, state_province):
        provice_filter = {
            'ISOcode': ConvertCountry(country_code).three_letter_code
        }

        province_list = self.endpoint.client.post('Types/GetProvinces', provice_filter)
        if province_list['RecordsCounter'] == 0:
            return ''
        province = [p for p in province_list['Records'] if
                    p['Name'] == state_province]
        return province[0]

    async def check_shipping_state(self):
        order_response = self.endpoint.client.post('Orders/GetOrders', {'Id': int(self.endpoint_order_number)})
        if order_response['Records'][0]:
            order = order_response['Records'][0]
            if order['OrderStatus']['Id'] == self.endpoint.endpoint_options['closed_order_status_id']:
                if len(order['OrderShipments']) > 0:
                    self.tracking_code = order['OrderShipments'][0]['Barcode']
                    self.tracking_url = order['OrderShipments'][0]['TrackTraceUrl']
                self.update_tracking()
                self.set_order_state('SHIPPED')
