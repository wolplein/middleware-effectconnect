from middleware.products.models.product_category import ProductCategories
from middleware.products.models.product_category import ProductCategory


class Logic4ProductCategories(ProductCategories):
    def __init__(self, endpoint):
        super().__init__()
        self.endpoint = endpoint

    def import_categories(self, channel):
        logic4_product_categories = self.endpoint.client.post('ProductGroups/GetProductGroups', {
            "IsVisibleOnWebShop": True,
            "ParentId": channel.import_category
        })
        for logic4_product_category in logic4_product_categories['Records']:
            product_category = ProductCategory()
            product_category.id = logic4_product_category['Id']
            product_category.name = logic4_product_category['Name']
            product_category.channel_id = channel.channel_id
            if product_category.not_exist():
                product_category.insert_category()
