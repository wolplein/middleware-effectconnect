from middleware.products.models.product import Product
from middleware.products.models.product import ListOfProducts
from middleware.products.models.product_channel_details import ProductChannelDetails
from middleware.products.models.product_channel_details_images import ProductChannelDetailsImages
from middleware.products.models.product_channel_details_translations import ProductChannelDetailsTranslation
from middleware.channel_endpoint_options import channel_endpoint_option_list


class Logic4Product(Product):
    def __init__(self, endpoint):
        super().__init__()
        self.endpoint = endpoint

    def get_pricelist_price(self, pricelist_id):
        pricelist_filter = {
            "PricelistId": pricelist_id,
            "LoadContractPrices": True
        }
        pricelist_response = self.endpoint.client.post('Products/GetPricelists', pricelist_filter)
        if pricelist_response['Records']:
            prices = pricelist_response['Records'][0]['ContractPrices']
            price_dict = next(iter(item for item in prices if item['ProductId'] == self.endpoint_product_id), None)
            if price_dict:
                return price_dict['PriceEx']
            else:
                return False

    def get_translations(self, language_dict):
        seo_information_filter = {
            "ProductId": self.endpoint_product_id,
            "GlobalizationId": language_dict['GlobalizationId'],
            "WebsiteDomainId": language_dict['WebsiteDomainId']
        }
        seo_information_response = self.endpoint.client.post('Products/GetProductSEOInformations', seo_information_filter)
        if seo_information_response['Records']:
            name = seo_information_response['Records'][0]['Title']
            description = seo_information_response['Records'][0]['Description']
            return name, description


class Logic4ListOfProducts(ListOfProducts):
    def __init__(self, endpoint):
        super().__init__()
        self.endpoint = endpoint

    def import_stock(self):
        product_skus = self.get_external_product_skus()
        for sku in product_skus:
            product_stock_information = self.endpoint.client.post('Stock/GetStockInformationForProduct',
                                                                  {"ProductCode": sku})
            product = Logic4Product(self.endpoint)
            product.stock_endpoint_id = self.endpoint.endpoint_id
            product.sku = sku
            product.update_quantity(int(product_stock_information["Value"]["FreeStock"]))
        composed_product_ids = self.get_external_product_ids(compsed=True)
        for product_id in composed_product_ids:
            recipe = self.endpoint.client.post('Products/GetComposedProductComposition', product_id)
            if recipe['Records']:
                self.update_available_to_promise_recipe(product_id, recipe)
        assembly_product_ids = self.get_external_product_ids(assembly=True)
        for product_id in assembly_product_ids:
            recipe = self.endpoint.client.post('Products/GetProductAssemblyRecipe', product_id)
            if recipe['Records']:
                self.update_available_to_promise_recipe(product_id, recipe)

    def update_available_to_promise_recipe(self, product_id, recipe):
        possible_to_make = 999
        for bom_line in recipe['Records']:
            if bom_line['IsStockBasedProduct']:
                bom_stock_filter = {"ProductCode": bom_line["ProductCode"]}
                bom_product_stock_information = self.endpoint.client.post('Stock/GetStockInformationForProduct',
                                                                          bom_stock_filter)
                bom_free_stock = bom_product_stock_information["Value"]["FreeStock"]
            else:
                bom_free_stock = 999.0

            if bom_line["IsStockBasedProduct"]:
                product_qty_available = bom_free_stock // int(bom_line["Qty"])
                if product_qty_available < possible_to_make:
                    possible_to_make = product_qty_available
        product = Logic4Product(self.endpoint)
        product.stock_endpoint_id = self.endpoint.endpoint_id
        product.endpoint_product_id = product_id
        product.load_sku_endpoint_product_id()
        product.update_quantity(possible_to_make)


    def import_products(self, category_id, channel):
        logic4_products = self.endpoint.client.post('Products/GetProducts', {'ProductGroupId': category_id,
                                                                             'UseECommerceProductGroups': True})
        if 'Records' in logic4_products:
            channel_endpoint_options = channel_endpoint_option_list.get_options_by_channel_and_endpoint(
                channel.channel_id, self.endpoint.endpoint_id)
            for logic4_product in logic4_products['Records']:
                product = Logic4Product(self.endpoint)
                product.sku = logic4_product['ProductCode']
                product.stock_endpoint_id = self.endpoint.endpoint_id
                if product.not_exist():
                    product.endpoint_product_id = logic4_product['ProductId']
                    product.barcode = logic4_product['BarCode1']
                    product.brand = logic4_product['Brandname']
                    product.assembly = logic4_product['IsAssemblyProduct']
                    if logic4_product['IsComposedProduct'] and not logic4_product['IsAssemblyProduct']:
                        product.composed = logic4_product['IsComposedProduct']
                    product.insert_product()
                product_channel_details = ProductChannelDetails()
                product_channel_details.channel_product_identifier = channel.product_prefix + '_' + product.sku
                if product_channel_details.not_exist():
                    product_channel_details.price = round(
                        logic4_product["SellPriceGross"] * (1 + (logic4_product['VatPercent'] / 100)), 2)
                    pricelist_price = product.get_pricelist_price(channel.pricelist_id)
                    if pricelist_price:
                        product_channel_details.price = pricelist_price
                    product_channel_details.channel_id = channel.channel_id
                    product_channel_details.product_id = product.product_id
                    product_channel_details.category_id = category_id
                    product_channel_details.insert_product_channel_details()
                i = 1
                while i <= 3:
                    index = 'ImageUrl' + str(i)
                    if logic4_product[index]:
                        product_channel_details_images = ProductChannelDetailsImages()
                        product_channel_details_images.product_channel_details_id = product_channel_details.id
                        product_channel_details_images.image_url = logic4_product[index]
                        product_channel_details_images.order = i
                        if product_channel_details_images.not_exist():
                            product_channel_details_images.insert_product_channel_details_images()
                    i += 1
                for language in channel_endpoint_options['languages']:
                    product_channel_details_translation = ProductChannelDetailsTranslation()
                    product_channel_details_translation.name, product_channel_details_translation.description = \
                        product.get_translations(language)
                    product_channel_details_translation.language = language['lang']
                    product_channel_details_translation.product_channel_details_id = product_channel_details.id
                    if product_channel_details_translation.not_exist():
                        product_channel_details_translation.insert_product_channel_details_translation()
