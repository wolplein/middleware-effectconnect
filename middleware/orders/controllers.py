import logging
from quart import Blueprint, jsonify
#from middleware.cron import cron
from middleware.mail import mail
from middleware.endpoints import endpointlist
from middleware.channels import channellist
from .models.order import ListOfOrders


_logger = logging.getLogger(__name__)

order = Blueprint('orders', __name__)


@order.route('/import_orders')
async def import_orders():
    for channel in channellist.CHANNEL_LIST:
        if channel.order_endpoint:
            list_of_orders = channel.get_list_orders()
            list_of_orders.get_open_orders()
    return jsonify({'ok': 'ok'})


@order.route('/export_orders')
async def export_orders():
    list_of_orders = ListOfOrders()
    list_of_orders.get_order_list_by_state('NEW')
    for order in list_of_orders.order_list:
        endpoint = endpointlist.get_endpoint_by_id(channellist.get_channel_by_id(order.channel_id).order_endpoint)
        endpoint_order = endpoint.get_order()
        endpoint_order.order_id = order.order_id
        await endpoint_order.load()
        await endpoint_order.export_order()
    return jsonify({'ok': 'ok'})


@order.route('/complete_orders')
async def complete_orders():
    for channel in channellist.CHANNEL_LIST:
        if channel.order_endpoint:
            list_of_orders = channel.get_list_orders()
            list_of_orders.get_order_list_by_state_full('SHIPPED')
            if len(list_of_orders.order_list) > 0:
                list_of_orders.complete_orders_channel()
                for order in list_of_orders.order_list:
                    order.set_order_state('DONE')
    return jsonify({'ok': 'ok'})


@order.route('/shipping_state')
async def shipping_state():
    list_of_orders = ListOfOrders()
    list_of_orders.get_order_list_by_state('EXPORTED')
    for order in list_of_orders.order_list:
        endpoint = endpointlist.get_endpoint_by_id(channellist.get_channel_by_id(order.channel_id).order_endpoint)
        endpoint_order = endpoint.get_order()
        endpoint_order.order_id = order.order_id
        await endpoint_order.load()
        await endpoint_order.check_shipping_state()
    return jsonify({'ok': 'ok'})


@order.route('/order_number_update')
async def order_number_update():
    for channel in channellist.CHANNEL_LIST:
        if channel.order_endpoint:
            list_of_orders = channel.get_list_orders()
            list_of_orders.get_order_list_by_state('EXPORTED_ENDPOINT')
            if len(list_of_orders.order_list) > 0:
                list_of_orders.update_endpoint_order_numbers_channel()
                for order in list_of_orders.order_list:
                    order.set_order_state('EXPORTED')
    return jsonify({'ok': 'ok'})


@order.route('/mail_error_log')
async def mail_error_log():
    list_of_orders = ListOfOrders()
    list_of_orders.get_order_ids_by_state_like('ERROR')
    if list_of_orders.order_list:
        subject = 'Middleware effectconnect errors'
        message = ""
        for order in list_of_orders.order_list:
            message += "order_id: {0}\tstatus: {1}\n\r".format(str(order.order_id), order.state)
        mail.send_mail('support@more2make.nl', subject, message)
    return jsonify({'ok': 'ok'})
