from database import database


class OrderItem:
    def __init__(self):
        self.order_item_id = 0
        self.order_id = 0
        self.channel_line_id = ""
        self.sku = ""
        self.barcode = ""
        self.product_description = ""
        self.price = 0.00
        self.quantity = 0
        self.external_product_id = 0

    def insert_order_item(self):
        query = """
                          INSERT INTO order_items(
                              order_id, channel_line_id, sku, barcode, product_description, price, quantity, 
                              external_product_id
                          )
                          VALUES (
                              {order_id}, '{channel_line_id}', '{sku}', {barcode}, {product_description},
                              {price}, {quantity}, {external_product_id}
                          )"""
        query = query.format(
            order_id=self.order_id,
            channel_line_id=database.db.db_connection.escape_string(self.channel_line_id),
            sku=database.db.db_connection.escape_string(self.sku),
            barcode=database.db.string_or_null(self.barcode),
            product_description=database.db.string_or_null(self.product_description),
            price=self.price,
            quantity=self.quantity,
            external_product_id=self.external_product_id
        )
        self.order_item_id = database.db.no_select_query(query, return_id=True)

