from database import database
from .order_item import OrderItem
from middleware.customers.models.customer import Customer


class Order:
    def __init__(self):
        self.order_id = 0
        self.channel_id = 0
        self.endpoint_order_number = False
        self.channel_order_number = False
        self.shipping_customer = False
        self.billing_customer = False
        self.order_date = False
        self.tracking_code = False
        self.tracking_url = False
        self.type = False
        self.subtype = False
        self.state = 'NEW'
        self.order_items = []

    def not_exist(self):
        query = """
                SELECT order_id from orders WHERE channel_order_number = '{channel_order_number}'
                """.format(channel_order_number=database.db.db_connection.escape_string(self.channel_order_number))
        data = database.db.fetchall(query)
        if len(data) == 0:
            return True
        else:
            return False

    def import_order(self, channel_order_data):
        pass

    def insert_new_order(self):
        query = """
                    INSERT INTO orders (channel_id, channel_order_number, order_date, state, type, subtype)
                    VALUES ({channel_id}, '{channel_order_number}', {order_date}, '{state}', '{type}', '{subtype}')

        """
        query = query.format(
            channel_id=self.channel_id,
            channel_order_number=database.db.db_connection.escape_string(self.channel_order_number),
            order_date=database.Database.datetime_to_sql(self.order_date),
            state=database.db.db_connection.escape_string(self.state),
            type=database.db.db_connection.escape_string(self.type),
            subtype=database.db.db_connection.escape_string(self.subtype),
        )
        self.order_id = database.db.no_select_query(query, return_id=True)

    def update_order_customers(self):
        if self.order_id:
            update_query = """ UPDATE orders SET
                                    shipping_customer_id={shipping_customer_id},
                                    billing_customer_id={billing_customer_id}
                                    WHERE order_id = {order_id}                
                """
            if self.billing_customer:
                billing_customer_id = self.billing_customer.customer_id
            else:
                billing_customer_id = False
            update_query = update_query.format(
                shipping_customer_id=self.shipping_customer.customer_id,
                billing_customer_id=database.db.int_or_null(billing_customer_id),
                order_id=self.order_id
            )
            database.db.no_select_query(update_query)

    def update_tracking(self):
        if self.order_id:
            update_query = """ UPDATE orders SET
                                    tracking_code='{tracking_code}',
                                    tracking_url='{tracking_url}'
                                    WHERE order_id = {order_id}                
                """
            update_query = update_query.format(
                tracking_code=self.tracking_code,
                tracking_url=self.tracking_url,
                order_id=self.order_id
            )
            database.db.no_select_query(update_query)

    def set_order_state(self, state):
        self.state = state
        if self.order_id:
            update_query = """UPDATE orders SET state='{state}' WHERE order_id = {order_id}"""
            update_query = update_query.format(
                state=database.db.db_connection.escape_string(self.state),
                order_id=self.order_id
            )
            database.db.no_select_query(update_query)

    async def load(self):
        if self.order_id:
            self.load_from_db()
            return True

    def load_from_db(self):
        if self.order_id:
            data = database.db.fetchall("SELECT * FROM orders WHERE order_id = " + str(self.order_id))
            if len(data) == 1:
                order_data = data[0]
                self.order_id = order_data["order_id"]
                self.channel_id = order_data["channel_id"]
                self.endpoint_order_number = order_data["endpoint_order_number"]
                self.channel_order_number = order_data["channel_order_number"]

                self.shipping_customer = Customer()
                self.shipping_customer.customer_id = order_data["shipping_customer_id"]
                self.shipping_customer.load_from_db()

                self.billing_customer = Customer()
                self.billing_customer.customer_id = order_data["billing_customer_id"]
                self.billing_customer.load_from_db()

                self.tracking_code = order_data["tracking_code"]
                self.tracking_url = order_data["tracking_url"]
                self.state = order_data["state"]

                self.type = order_data['type']
                self.subtype = order_data['subtype']

                order_items_data = database.db.fetchall("SELECT * FROM order_items WHERE order_id = " + str(self.order_id))
                self.order_items = []
                for order_item_row in order_items_data:
                    order_item = OrderItem()
                    order_item.database_id = order_item_row['item_id']
                    order_item.order_id = order_item_row['order_id']
                    order_item.channel_line_id = order_item_row['channel_line_id']
                    order_item.sku = order_item_row['sku']
                    order_item.barcode = order_item_row['barcode']
                    order_item.product_description = order_item_row['product_description']
                    order_item.quantity = order_item_row['quantity']
                    order_item.price = order_item_row['price']
                    order_item.external_product_id = order_item_row['external_product_id']
                    self.order_items.append(order_item)

    def update_order_endpoint_order_number(self):
        if self.order_id:
            update_query = """ UPDATE orders SET endpoint_order_number = '{endpoint_order_number}', state='{state}' 
                               WHERE order_id = {order_id}                
                """

            update_query = update_query.format(
                endpoint_order_number=database.db.db_connection.escape_string(str(self.endpoint_order_number)),
                state=database.db.db_connection.escape_string(self.state),
                order_id=self.order_id
            )
            database.db.no_select_query(update_query)

    def load_for_channel_update(self):
        if self.order_id:
            data = database.db.fetchall("SELECT endpoint_order_number, channel_order_number FROM orders WHERE order_id = " + str(self.order_id))
            order_data = data[0]
            self.channel_order_number = order_data['channel_order_number']
            self.endpoint_order_number = order_data['endpoint_order_number']

    async def check_shipping_state(self):
        return False


class ListOfOrders:
    def __init__(self):
        self.order_list = []

    def get_open_orders(self):
        pass

    def get_order_list_by_state(self, state):
        data = database.db.fetchall("SELECT order_id, channel_id FROM orders WHERE state = '" + str(state) + "'")
        if data:
            for row in data:
                order = Order()
                order.order_id = row['order_id']
                order.channel_id = row['channel_id']
                self.order_list.append(order)
            return True
        else:
            self.order_list = []
            return False

    def get_order_list_by_state_full(self, state):
        data = database.db.fetchall("SELECT order_id, channel_id FROM orders WHERE state = '" + str(state) + "'")
        if data:
            for row in data:
                order = Order()
                order.order_id = row['order_id']
                order.load_from_db()
                self.order_list.append(order)
            return True
        else:
            self.order_list = []
            return False

    def get_order_ids_by_state_like(self, state):
        data = database.db.fetchall("SELECT order_id, state FROM orders WHERE state like '%" + str(state) + "%'")
        if data:
            for row in data:
                order = Order()
                order.order_id = row['order_id']
                order.state = row['state']
                self.order_list.append(order)
            return True
        else:
            self.order_list = []
            return False

    def update_endpoint_order_numbers_channel(self):
        pass

    def complete_orders_channel(self):
        pass
