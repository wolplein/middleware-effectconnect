from database import database
import importlib
from .channel import Channel


def load_channel_list():
    data = database.db.fetchall("SELECT * FROM channels where active = 1")
    channel_list = []
    for row in data:
        channel_code = row['channel_code']
        channel_module = importlib.import_module(
            'middleware.channels.' + channel_code + '.' + channel_code + '_channel')
        tmp_channel = getattr(channel_module, channel_dict[channel_code] + 'Channel')()
        if not tmp_channel:
            tmp_channel = Channel()

        tmp_channel.channel_id = row['channel_id']
        tmp_channel.name = row['name']
        tmp_channel.product_prefix = row['product_prefix']
        tmp_channel.url = row['url']
        tmp_channel.public_key = row['public_key']
        tmp_channel.private_key = row['private_key']
        tmp_channel.channel_code = channel_code
        tmp_channel.product_data_endpoint = row['product_data_endpoint']
        if tmp_channel.product_data_endpoint:
            tmp_channel.import_category = row['import_category']
        tmp_channel.product_stock_endpoint = row['product_stock_endpoint']
        tmp_channel.product_price_endpoint = row['product_price_endpoint']
        if tmp_channel.product_price_endpoint:
            tmp_channel.pricelist_id = row['pricelist_id']
        tmp_channel.order_endpoint = row['order_endpoint']

        client_module = importlib.import_module('middleware.channels.' + channel_code + '.' + channel_code + '_client')
        tmp_channel.client = getattr(client_module, channel_dict[channel_code] + 'Client')(tmp_channel)

        channel_list.append(tmp_channel)
    return channel_list


def get_channel_by_id(channel_id):
    for channel in CHANNEL_LIST:
        if channel.channel_id == channel_id:
            return channel
    raise Exception("No channel found for id " + str(channel_id))


def get_channel_by_product_channel_code(product_channel_code):
    for channel in CHANNEL_LIST:
        if channel.product_channel_code == product_channel_code:
            return channel
    raise Exception("No channel found for product_channel_code " + str(product_channel_code))


def get_channel_by_channel_code(channel_code):
    for channel in CHANNEL_LIST:
        if channel.channel_code == channel_code:
            return channel
    raise Exception("No channel found for channel_code " + str(channel_code))


channel_dict = {
    'effectconnect': 'EffectConnect'
}

CHANNEL_LIST = load_channel_list()
