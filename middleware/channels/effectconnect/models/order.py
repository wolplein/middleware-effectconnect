import os
import dateutil.parser
from datetime import datetime
from lxml import etree
from middleware.customers.models.customer import Customer
from middleware.orders.models.order_item import OrderItem
from middleware.orders.models.order import Order
from middleware.orders.models.order import ListOfOrders


class EffectConnectOrder(Order):
    def __init__(self, channel):
        super().__init__()
        self.channel = channel

    def import_order(self, order_data):
        self.channel_order_number = order_data['identifiers']['channelNumber']
        if self.not_exist():
            self.channel_id = self.channel.channel_id
            self.state = 'NEW'
            self.order_date = dateutil.parser.parse(order_data['date'])
            self.type = order_data['channelInfo']['type']
            self.subtype = order_data['channelInfo']['subtype']
            self.insert_new_order()
            if order_data.get('billingAddress', False):
                billing_customer = Customer()
                if order_data['billingAddress'].get('firstName', False):
                    billing_customer.firstname = order_data['billingAddress']['firstName']
                if order_data['billingAddress'].get('lastName', False):
                    billing_customer.lastname = order_data['billingAddress']['lastName']
                if order_data['billingAddress'].get('company', False):
                    billing_customer.company_name = order_data['billingAddress']['company']
                if order_data['billingAddress'].get('street', False):
                    billing_customer.street = order_data['billingAddress']['street']
                if order_data['billingAddress'].get('houseNumber', False):
                    billing_customer.housenumber = str(order_data['billingAddress']['houseNumber'])
                if order_data['billingAddress'].get('houseNumberExtension', False):
                    billing_customer.housenumber_addition = order_data['billingAddress']['houseNumberExtension']
                if order_data['billingAddress'].get('addressNote', False):
                    billing_customer.street2 = order_data['billingAddress']['addressNote']
                if order_data['billingAddress'].get('zipCode', False):
                    billing_customer.zipcode = order_data['billingAddress']['zipCode']
                if order_data['billingAddress'].get('city', False):
                    billing_customer.city = order_data['billingAddress']['city']
                if order_data['billingAddress'].get('state', False):
                    billing_customer.state_province = order_data['billingAddress']['state']
                if order_data['billingAddress'].get('country', False):
                    billing_customer.country_code = order_data['billingAddress']['country']
                if order_data['billingAddress'].get('phone', False):
                    billing_customer.telephone = order_data['billingAddress']['phone']
                if order_data['billingAddress'].get('email', False):
                    billing_customer.email = order_data['billingAddress']['email']
                billing_customer.order_id = self.order_id
                billing_customer.type = 'BILLING'
                billing_customer.insert_customer()
                self.billing_customer = billing_customer
            if order_data.get('shippingAddress', False):
                shipping_customer = Customer()
                if order_data['shippingAddress'].get('firstName', False):
                    shipping_customer.firstname = order_data['shippingAddress']['firstName']
                if order_data['shippingAddress'].get('lastName', False):
                    shipping_customer.lastname = order_data['shippingAddress']['lastName']
                if order_data['shippingAddress'].get('company', False):
                    shipping_customer.company_name = order_data['shippingAddress']['company']
                if order_data['shippingAddress'].get('street', False):
                    shipping_customer.street = order_data['shippingAddress']['street']
                if order_data['shippingAddress'].get('houseNumber', False):
                    shipping_customer.housenumber = str(order_data['shippingAddress']['houseNumber'])
                if order_data['shippingAddress'].get('houseNumberExtension', False):
                    shipping_customer.housenumber_addition = order_data['shippingAddress']['houseNumberExtension']
                if order_data['shippingAddress'].get('addressNote', False):
                    shipping_customer.street2 = order_data['shippingAddress']['addressNote']
                if order_data['shippingAddress'].get('zipCode', False):
                    shipping_customer.zipcode = order_data['shippingAddress']['zipCode']
                if order_data['shippingAddress'].get('city', False):
                    shipping_customer.city = order_data['shippingAddress']['city']
                if order_data['shippingAddress'].get('state', False):
                    shipping_customer.state_province = order_data['shippingAddress']['state']
                if order_data['shippingAddress'].get('country', False):
                    shipping_customer.country_code = order_data['shippingAddress']['country']
                if order_data['shippingAddress'].get('phone', False):
                    shipping_customer.telephone = order_data['shippingAddress']['phone']
                if order_data['shippingAddress'].get('email', False):
                    shipping_customer.email = order_data['shippingAddress']['email']
                shipping_customer.order_id = self.order_id
                shipping_customer.type = 'SHIPPING'
                shipping_customer.insert_customer()
                self.shipping_customer = shipping_customer
            self.update_order_customers()
            #Todo Check sku, ean, ideentifer als product import in effectconnect is gelukt

            try:
                lines = sorted(set(map(lambda x: x['product']['SKU'], order_data['lines'])))

                combined_lines = [{'sku': sku,
                                   'line_amount': sum(map(self.get_line_amount(sku), order_data['lines'])),
                                   'qty': sum(map(self.get_qty(sku), order_data['lines'])),
                                   'extra_info': self.get_other_line_info(order_data['lines'], sku)
                                   } for sku in lines]

                for item in combined_lines:
                    order_item = OrderItem()
                    order_item.order_id = self.order_id
                    order_item.channel_line_id = item['extra_info']['channel_line_id']
                    order_item.sku = item['sku']
                    order_item.barcode = item['extra_info']['ean']
                    order_item.product_description = item['extra_info']['name']
                    order_item.quantity = item['qty']
                    order_item.price = item['line_amount']
                    order_item.insert_order_item()
                    self.order_items.append(order_item)
            except:
                self.set_order_state('IMPORT_ERROR')

    @staticmethod
    def get_line_amount(sku):
        return lambda x: x['lineAmount'] if x['product']['SKU'] == sku else 0

    @staticmethod
    def get_qty(sku):
        return lambda x: 1 if x['product']['SKU'] == sku else 0

    @staticmethod
    def get_other_line_info(data, sku):
        extra_info = {}
        for item in data:
            if item['product']['SKU'] == sku:
                extra_info['name'] = item['productTitle']
                extra_info['ean'] = item['product']['EAN']
                extra_info['channel_line_id'] = item['identifiers']['channelLineId']
                break
        return extra_info


class EffectConnectListOfOrders(ListOfOrders):
    def __init__(self, channel):
        super().__init__()
        self.channel = channel

    def get_open_orders(self):
        file_directory = os.path.dirname(os.path.abspath(__file__))
        filename = 'orderlist_read_' + str(datetime.now().strftime('%Y%m%d%H%M%S')) + '.xml'
        file = os.path.join(file_directory, 'tmp', filename)
        list = etree.Element("list")
        filters = etree.SubElement(list, "filters")
        has_status_filter = etree.SubElement(filters, 'hasStatusFilter')
        etree.SubElement(has_status_filter, "filterValue").text = "paid"
        tree = etree.ElementTree(list)
        tree.write(file, encoding='utf-8', xml_declaration=True)

        size = os.stat(file).st_size
        with open(file, 'rb') as payload:
            response = self.channel.client.get(payload, size, '/orderlist', '2.0')
            if response:
                if response['Response']['OrderListReadResponseContainer']['Count'] > 0:
                    for response_order in response['Response']['OrderListReadResponseContainer']['Orders']:
                        ec_order = EffectConnectOrder(self.channel)
                        ec_order.import_order(response_order)

    def update_endpoint_order_numbers_channel(self):
        file_directory = os.path.dirname(os.path.abspath(__file__))
        filename = 'order_update_endpoint_numbers_' + str(datetime.now().strftime('%Y%m%d%H%M%S')) + '.xml'
        file = os.path.join(file_directory, 'tmp', filename)
        update = etree.Element("update")
        orders_xml = etree.SubElement(update, "orders")
        for order in self.order_list:
            order.load_for_channel_update()
            order_update = etree.SubElement(orders_xml, "orderUpdate")
            etree.SubElement(order_update, "orderIdentifierType").text = "channelNumber"
            etree.SubElement(order_update, "orderIdentifier").text = order.channel_order_number
            etree.SubElement(order_update, "connectionIdentifier").text = order.endpoint_order_number
            etree.SubElement(order_update, "connectionNumber").text = str(order.endpoint_order_number)
        tree = etree.ElementTree(update)
        tree.write(file, encoding='utf-8', xml_declaration=True)
        size = os.stat(file).st_size
        with open(file, 'rb') as payload:
            response = self.channel.client.put(payload, size, '/orders')

    def complete_orders_channel(self):
        file_directory = os.path.dirname(os.path.abspath(__file__))
        filename = 'order_update_ship_' + str(datetime.now().strftime('%Y%m%d%H%M%S')) + '.xml'
        file = os.path.join(file_directory, 'tmp', filename)
        update = etree.Element("update")
        order_lines = etree.SubElement(update, 'orderlines')
        for order in self.order_list:
            for item in order.order_items:
                line_update = etree.SubElement(order_lines, 'lineUpdate')
                etree.SubElement(line_update, "orderlineIdentifierType").text = "channelLineId"
                etree.SubElement(line_update, "orderlineIdentifier").text = item.channel_line_id
                etree.SubElement(line_update, "trackingNumber").text = order.tracking_code
                etree.SubElement(line_update, "trackingUrl").text = order.tracking_url
        tree = etree.ElementTree(update)
        tree.write(file, encoding='utf-8', xml_declaration=True)
        size = os.stat(file).st_size
        with open(file, 'rb') as payload:
            response = self.channel.client.put(payload, size, '/orders')