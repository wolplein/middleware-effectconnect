import os
from datetime import datetime
from lxml import etree
from middleware.products.models.product_channel_details import ListOfProductChannelDetails


class EffectConnectListOfProductChannelDetails(ListOfProductChannelDetails):
    def __init__(self, channel):
        super().__init__()
        self.channel = channel

    def export_products(self):
        file_directory = os.path.dirname(os.path.abspath(__file__))
        filename = 'product_create_' + str(datetime.now().strftime('%Y%m%d%H%M%S')) + '.xml'
        file = os.path.join(file_directory, 'tmp', filename)
        products = etree.Element("products")
        for row in self.list_of_pcds:
            product = etree.SubElement(products, 'product')
            etree.SubElement(product, "identifier").text = row.channel_product_identifier
            etree.SubElement(product, "brand").text = row.product.brand
            categories = etree.SubElement(product, 'categories')
            category = etree.SubElement(categories, 'category')
            category_titles = etree.SubElement(category, 'titles')
            etree.SubElement(category_titles, "title", attrib={'language': 'nl'}).text = row.category.name
            options = etree.SubElement(product, 'options')
            option = etree.SubElement(options, 'option')
            etree.SubElement(option, "identifier").text = row.channel_product_identifier
            etree.SubElement(option, "price").text = str(row.price)
            etree.SubElement(option, "stock").text = str(row.product.qty_available)
            option_titles = etree.SubElement(option, 'titles')
            descriptions = etree.SubElement(option, 'descriptions')
            for translation in row.product_channel_details_translations:
                etree.SubElement(option_titles, "title", attrib={'language': translation.language}).text = translation.name
                etree.SubElement(descriptions, "description", attrib={'language': translation.language}).text = etree.CDATA(translation.description)
            etree.SubElement(option, "ean").text = row.product.barcode
            etree.SubElement(option, "sku").text = row.product.sku
            images = etree.SubElement(option, 'images')
            for product_channel_detail_image in row.product_channel_details_images:
                image = etree.SubElement(images, 'image')
                etree.SubElement(image, "url").text = product_channel_detail_image.image_url
                etree.SubElement(image, "order").text = str(product_channel_detail_image.order)
        tree = etree.ElementTree(products)
        tree.write(file, encoding='utf-8', xml_declaration=True)
        size = os.stat(file).st_size
        with open(file, 'rb') as payload:
            response = self.channel.client.post(payload, size, '/products', file=True)

    def update_products(self):
        file_directory = os.path.dirname(os.path.abspath(__file__))
        filename = 'product_update_' + str(datetime.now().strftime('%Y%m%d%H%M%S')) + '.xml'
        file = os.path.join(file_directory, 'tmp', filename)
        products = etree.Element("products")
        for pcd in self.list_of_pcds:
            product = etree.SubElement(products, "product")
            etree.SubElement(product, "identifier").text = pcd.channel_product_identifier
            options = etree.SubElement(product, 'options')
            option = etree.SubElement(options, 'option')
            etree.SubElement(option, "identifier").text = pcd.channel_product_identifier
            etree.SubElement(option, "price").text = str(pcd.price)
            etree.SubElement(option, "stock").text = str(pcd.product.qty_available)
        tree = etree.ElementTree(products)
        tree.write(file, encoding='utf-8', xml_declaration=True)
        size = os.stat(file).st_size
        with open(file, 'rb') as payload:
            response = self.channel.client.put(payload, size, '/products', file=True)