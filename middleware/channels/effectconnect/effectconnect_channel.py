from middleware.channels.channel import Channel
from .models.product_channel_details import EffectConnectListOfProductChannelDetails
from .models.order import EffectConnectListOfOrders


class EffectConnectChannel(Channel):
    def get_list_of_product_channel_details(self):
        return EffectConnectListOfProductChannelDetails(self)

    def get_list_orders(self):
        return EffectConnectListOfOrders(self)
