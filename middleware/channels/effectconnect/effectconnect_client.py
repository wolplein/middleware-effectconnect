import json
import hmac
import requests
import datetime
import hashlib
import base64
import logging

_logger = logging.getLogger(__name__)


class EffectConnectClient:
    url = False,
    public_key = False
    private_key = False
    version = False

    def __init__(self, channel):
        self.url = channel.url
        self.public_key = channel.public_key
        self.private_key = channel.private_key

    def calc_signature(self, size, call_time, method, uri):
        hashed_string = [str(size), method, uri, self.version, str(call_time)]
        secret_key_byte_array = bytes(self.private_key, 'utf-8')
        hmac_string = hmac.new(secret_key_byte_array, bytes(''.join(hashed_string), 'utf-8'), digestmod=hashlib.sha512)
        hash_string = base64.b64encode(hmac_string.digest()).decode()
        return hash_string

    def get(self, payload, size, endpoint, v='2.0'):
        self.version = v
        call_time = datetime.datetime.now().replace(microsecond=0).isoformat()
        headers = {'KEY': self.public_key,
                   'VERSION': self.version,
                   'URI': endpoint,
                   'TIME': str(call_time),
                   'RESPONSETYPE': 'json',
                   'Content-Type': 'text/xml',
                   'SIGNATURE': self.calc_signature(size, call_time, 'GET', endpoint)
                   }
        response = requests.get(
            self.url + endpoint,
            data=payload,
            headers=headers
        )
        response_json = json.loads(response.content)
        if response_json['Response']['Result'] == 'Success':
            return response_json
        else:
            error_message = ''
            for error in response_json['ErrorContainer']['ErrorMessages']:
                error_message += '{0}: {1} \n\r'.format(error['Code'], error['Message'])
            _logger.error(str(error_message))
            raise Exception(error_message)

    def post(self, payload, size, endpoint, v='2.0', file=False):
        self.version = v
        call_time = datetime.datetime.now().replace(microsecond=0).isoformat()
        headers = {'KEY': self.public_key,
                   'VERSION': self.version,
                   'URI': endpoint,
                   'TIME': str(call_time),
                   'RESPONSETYPE': 'json',
                   'Content-Type': 'text/xml',
                   'SIGNATURE': self.calc_signature(size, call_time, 'POST', endpoint)
                   }
        kwargs = {}

        if file:
            kwargs['files'] = {'upload_file': payload}
        else:
            kwargs['data'] = payload

        response = requests.post(
            self.url + endpoint,
            headers=headers,
            **kwargs
        )
        response_json = json.loads(response.content)
        if response_json['Response']['Result'] == 'Success':
            return response_json
        else:
            error_message = ''
            for error in response_json['ErrorContainer']['ErrorMessages']:
                error_message += '{0}: {1} \n\r'.format(error['Code'], error['Message'])
            _logger.error(str(error_message))
            raise Exception(error_message)

    def put(self, payload, size, endpoint, v='2.0', file=False):
        self.version = v
        call_time = datetime.datetime.now().replace(microsecond=0).isoformat()
        headers = {'KEY': self.public_key,
                   'VERSION': self.version,
                   'URI': endpoint,
                   'TIME': str(call_time),
                   'RESPONSETYPE': 'json',
                   'Content-Type': 'text/xml',
                   'SIGNATURE': self.calc_signature(size, call_time, 'PUT', endpoint)
                   }
        kwargs = {}

        if file:
            kwargs['files'] = {'upload_file': payload}
        else:
            kwargs['data'] = payload

        response = requests.put(
            self.url + endpoint,
            headers=headers,
            **kwargs
        )
        response_json = json.loads(response.content)
        if response_json['Response']['Result'] == 'Success':
            return response_json
        else:
            error_message = ''
            for error in response_json['ErrorContainer']['ErrorMessages']:
                error_message += '{0}: {1} \n\r'.format(error['Code'], error['Message'])
            _logger.error(str(error_message))
            raise Exception(error_message)

