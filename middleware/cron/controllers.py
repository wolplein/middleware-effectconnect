import logging
from quart import Blueprint, jsonify
from middleware.cron import cron

_logger = logging.getLogger(__name__)
cron_controller = Blueprint('cron_controller', __name__)


@cron_controller.route('/reset_cron_by_reset_interval')
@cron.cron.cron
def cron_reset_cron_check():
    cron.reset_cron_by_reset_interval()
    return jsonify("Reset cron by interval")


@cron_controller.route('/clean_up_tmp_dir')
@cron.cron.cron
def clean_up_tmp_dir():
    cron.clean_up_tmp_dir()
    return jsonify("tmp dir cleaned")