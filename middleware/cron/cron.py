import os
from functools import wraps
from database import database
from datetime import datetime
from quart import Blueprint, jsonify


class Cron(object):
    def cron(self, f):
        @wraps(f)
        async def decorated(*args, **kwargs):
            if self.check_running(f.__name__):
                self.set_date("start", f.__name__)
                value = await f(*args, **kwargs)
                self.set_date("end", f.__name__)
                return value
            else:
                return jsonify("Cron is already running, skipped")

        return decorated

    @staticmethod
    def check_running(cron_function_name):
        cron_query = """SELECT id FROM cron_schedule WHERE method='{cron_function_name}' AND is_running = 0"""
        cron_query = cron_query.format(cron_function_name=cron_function_name)
        data = database.db.fetchall(cron_query)
        if data:
            return True
        return False

    @staticmethod
    def set_date(date_type, cron_function_name):
        if date_type == "start":
            is_running = 1
        else:
            is_running = 0
        update_query = """UPDATE cron_schedule SET last_run_{date_type} = {date}, is_running = {is_running}
        WHERE method='{cron_function_name}'"""
        update_query = update_query.format(date_type=date_type, cron_function_name=cron_function_name,
                                           date=database.Database.datetime_to_sql(datetime.now()),
                                           is_running=is_running)
        database.db.no_select_query(update_query)


def reset_cron():
    database.db.no_select_query("UPDATE cron_schedule  SET is_running = 0 WHERE is_running = 1")


def reset_cron_by_reset_interval():
    update_query = "UPDATE cron_schedule SET is_running = 0, last_reset = NOW() " \
                   "WHERE NOW() >= DATE_ADD(last_run_start, INTERVAL reset_time MINUTE) AND is_running = 1"
    database.db.no_select_query(update_query)


def clean_up_tmp_dir():
    middleware_dir = os.path.abspath(os.path.join(os.path.dirname(os.path.abspath(__file__)), os.pardir))
    for root, dirs, files in os.walk(str(middleware_dir), topdown=False):
        if 'tmp' in root:
            for tmp_root, tmp_dirs, tmp_files in os.walk(root):
                for file in tmp_files:
                    os.remove(os.path.join(tmp_root, file))


reset_cron()

cron = Cron()
