import logging
from quart import Blueprint, jsonify
#from middleware.cron import cron
from .models.product_channel_details import ListOfProductChannelDetails
from middleware.endpoints import endpointlist
from middleware.channels import channellist

_logger = logging.getLogger(__name__)

product = Blueprint('products', __name__)


@product.route('/import_product_categories')
async def import_product_categories():
    for channel in channellist.CHANNEL_LIST:
        if channel.product_data_endpoint:
            endpoint = endpointlist.get_endpoint_by_id(channel.product_data_endpoint)
            product_categories = endpoint.get_product_categories()
            product_categories.import_categories(channel)
    return jsonify({"ok": "ok"})


@product.route('/import_products')
async def import_products():
    for channel in channellist.CHANNEL_LIST:
        if channel.product_data_endpoint:
            endpoint = endpointlist.get_endpoint_by_id(channel.product_data_endpoint)
            product_categories = endpoint.get_product_categories()
            product_categories.load_list_of_categories_by_channel_id(channel.channel_id)
            for product_category in product_categories.list_of_categories:
                list_of_products = endpoint.get_list_of_products()
                list_of_products.import_products(product_category.id, channel)
    return jsonify({"ok": "ok"})


@product.route('/import_prices')
async def import_prices():
    for channel in channellist.CHANNEL_LIST:
        if channel.product_price_endpoint:
            endpoint = endpointlist.get_endpoint_by_id(channel.product_price_endpoint)
            list_of_product_channel_details = endpoint.get_list_of_product_channel_details()
            list_of_product_channel_details.channel_id = channel.channel_id
            list_of_product_channel_details.get_list_per_channel()
            list_of_product_channel_details.import_prices(channel.pricelist_id)
    return jsonify({"ok": "ok"})


@product.route('/import_stock')
async def import_stock():
    for endpoint in endpointlist.ENDPOINT_LIST:
        list_of_products = endpoint.get_list_of_products()
        list_of_products.import_stock()
    return jsonify({"ok": "ok"})


@product.route('/export_products')
async def export_products():
    for channel in channellist.CHANNEL_LIST:
        if channel.product_stock_endpoint:
            list_of_product_channel_details = channel.get_list_of_product_channel_details()
            list_of_product_channel_details.get_export_list()
            list_of_product_channel_details.export_products()
    return jsonify({"ok": "ok"})


@product.route('/update_products')
async def update_products():
    for channel in channellist.CHANNEL_LIST:
        if channel.product_stock_endpoint:
            list_of_product_channel_details = channel.get_list_of_product_channel_details()
            list_of_product_channel_details.get_export_list()
            list_of_product_channel_details.update_products()
    return jsonify({"ok": "ok"})
