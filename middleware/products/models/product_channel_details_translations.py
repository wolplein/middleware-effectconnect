from database import database


class ProductChannelDetailsTranslation:
    def __init__(self):
        self.id = 0
        self.product_channel_details_id = 0
        self.language = False
        self.name = False
        self.description = False

    def not_exist(self):
        query = """
            SELECT id FROM product_channel_details_translations 
            WHERE product_channel_details_id = {product_channel_details_id} and language = '{language}'
        """.format(product_channel_details_id=self.product_channel_details_id,
                   language=database.db.db_connection.escape_string(self.language))
        data = database.db.fetchall(query)
        if len(data) == 1:
            self.id = data[0]['id']
            self.update_product_channel_details_translation()
        elif len(data) == 0:
            return True
        else:
            raise 'Duplicated combination in product channel details translations {0} {1}'.format(
                str(self.product_channel_details_id), self.language)

    def insert_product_channel_details_translation(self):
        query = """
            INSERT INTO product_channel_details_translations (product_channel_details_id, language, name, description)
            VALUES ({product_channel_details_id}, '{language}', '{name}', '{description}')
        """
        query = query.format(product_channel_details_id=self.product_channel_details_id,
                             language=database.db.db_connection.escape_string(self.language),
                             name=database.db.db_connection.escape_string(self.name),
                             description=database.db.db_connection.escape_string(self.description)
                             )
        self.id = database.db.no_select_query(query, return_id=True)

    def update_product_channel_details_translation(self):
        query = """
            UPDATE product_channel_details_translations set name = '{name}', description = '{description}'
            WHERE id = {id}
        """
        query = query.format(name=database.db.db_connection.escape_string(self.name),
                             description=database.db.db_connection.escape_string(self.description),
                             id=self.id)
        database.db.no_select_query(query)


class ListOfProductChannelDetailsTranslations:
    def __init__(self):
        self.list_of_pcdst = []

    def load_list_of_product_channel_details_translations(self, product_channel_details_id):
        query = """SELECT * FROM product_channel_details_translations
        WHERE product_channel_details_id = {0}""".format(product_channel_details_id)
        data = database.db.fetchall(query)
        for row in data:
            product_channel_details_translation = ProductChannelDetailsTranslation()
            product_channel_details_translation.id = row['id']
            product_channel_details_translation.product_channel_details_id = row['product_channel_details_id']
            product_channel_details_translation.language = row['language']
            product_channel_details_translation.name = row['name']
            product_channel_details_translation.description = row['description']
            self.list_of_pcdst.append(product_channel_details_translation)