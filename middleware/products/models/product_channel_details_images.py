from database import database


class ProductChannelDetailsImages:
    def __init__(self):
        self.id = 0
        self.product_channel_details_id = 0
        self.image_url = False
        self.order = 0

    def not_exist(self):
        query = """
            SELECT id FROM product_channel_details_images 
            WHERE product_channel_details_id = {product_channel_details_id} and image_url = '{image_url}'
        """.format(product_channel_details_id=self.product_channel_details_id,
                   image_url=database.db.db_connection.escape_string(self.image_url))
        data = database.db.fetchall(query)
        if len(data) == 1:
            self.id = data[0]['id']
            self.update_product_channel_details_translation()
        elif len(data) == 0:
            return True
        else:
            raise 'Duplicated key in product channel details images {0} {1}'.format(
                str(self.product_channel_details_id))

    def insert_product_channel_details_images(self):
        query = """
                INSERT INTO product_channel_details_images (product_channel_details_id, image_url, product_channel_details_images.order)
                VALUES ({product_channel_details_id}, '{image_url}', {order})
            """
        query = query.format(product_channel_details_id=self.product_channel_details_id,
                             image_url=database.db.db_connection.escape_string(self.image_url),
                             order=self.order
                             )
        self.id = database.db.no_select_query(query, return_id=True)

    def update_product_channel_details_translation(self):
        query = """
            UPDATE product_channel_details_images set image_url = '{image_url}'
            WHERE id = {id}
        """
        query = query.format(image_url=database.db.db_connection.escape_string(self.image_url),
                             id=self.id)
        database.db.no_select_query(query)


class ListOfProductChannelDetailsImages:
    def __init__(self):
        self.list_of_pcdsi = []

    def load_list_of_product_channel_details_images(self, product_channel_details_id):
        query = """SELECT * FROM product_channel_details_images 
        WHERE product_channel_details_id = {0}""".format(product_channel_details_id)
        data = database.db.fetchall(query)
        for row in data:
            product_channel_details_image = ProductChannelDetailsImages()
            product_channel_details_image.id = row['id']
            product_channel_details_image.product_channel_details_id = row['product_channel_details_id']
            product_channel_details_image.image_url = row['image_url']
            product_channel_details_image.order = row['order']
            self.list_of_pcdsi.append(product_channel_details_image)