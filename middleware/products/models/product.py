from database import database


class Product:
    def __init__(self):
        self.product_id = 0
        self.endpoint_product_id = 0
        self.sku = False
        self.barcode = False
        self.brand = False
        self.qty_available = 0
        self.stock_endpoint_id = 0
        self.assembly = False
        self.composed = False

    def not_exist(self):
        query = """
             SELECT product_id from products WHERE sku = '{sku}' AND stock_endpoint_id={stock_endpoint_id}
             """.format(sku=self.sku, stock_endpoint_id=self.stock_endpoint_id)
        data = database.db.fetchall(query)
        if len(data) == 1:
            self.product_id = data[0]['product_id']
            self.load_from_db()
            return False
        elif len(data) == 0:
            return True
        else:
            raise 'Duplicated key in products {0}'.format(str(self.sku))

    def insert_product(self):
        query = """
               INSERT INTO products (stock_endpoint_id, endpoint_product_id, sku, barcode, brand, qty_available, 
               assembly, composed) 
               VALUES ({stock_endpoint_id}, {endpoint_product_id}, '{sku}', '{barcode}', '{brand}', {qty_available},
               {assembly}, {composed})
               """
        query = query.format(stock_endpoint_id=self.stock_endpoint_id,
                             endpoint_product_id=self.endpoint_product_id,
                             sku=database.db.db_connection.escape_string(self.sku),
                             barcode=database.db.db_connection.escape_string(self.barcode),
                             brand=database.db.db_connection.escape_string(self.brand),
                             qty_available=self.qty_available,
                             assembly=database.db.bool_to_db(self.assembly),
                             composed=database.db.bool_to_db(self.composed),
                             )
        self.product_id = database.db.no_select_query(query, return_id=True)

    def update_quantity(self, new_qty_available):
        self.qty_available = new_qty_available

        # Get current stock and check if product is in list
        query = """SELECT product_id, qty_available FROM products 
                            WHERE sku = '{sku}' AND stock_endpoint_id = {stock_endpoint_id}""".format(
            sku=self.sku,
            stock_endpoint_id=self.stock_endpoint_id)
        data = database.db.fetchall(query)
        if len(data) == 1:
            current_stock = data[0]['qty_available']
        else:
            return

        if new_qty_available != current_stock:
            # TODO: add last update datetime
            query = """
                           UPDATE products SET qty_available = {qty_available}
                           WHERE stock_endpoint_id = {stock_endpoint_id} AND sku = '{sku}'
                           """
            query = query.format(stock_endpoint_id=self.stock_endpoint_id,
                                 sku=database.db.db_connection.escape_string(self.sku),
                                 qty_available=self.qty_available
                                 )
            self.product_id = database.db.no_select_query(query)

    def load_from_db(self):
        data = database.db.fetchall('SELECT * FROM products where product_id = {0}'.format(self.product_id))
        for row in data:
            self.endpoint_product_id = row['endpoint_product_id']
            self.sku = row['sku']
            self.barcode = row['barcode']
            self.brand = row['brand']
            self.qty_available = row['qty_available']
            self.assembly = database.db.db_to_bool(row['assembly'])
            self.composed = database.db.db_to_bool(row['composed'])

    def load_sku_endpoint_product_id(self):
        data = database.db.fetchall('SELECT sku FROM products where endpoint_product_id = {0}'.format(
            self.endpoint_product_id))
        if len(data) == 1:
            self.sku = data[0]['sku']

class ListOfProducts:
    endpoint = False

    def get_external_product_ids(self):
        if self.endpoint:
            query = """
                         SELECT product_id from products where stock_endpoint_id = '{0}'
                         """.format(self.endpoint.endpoint_id)
            data = database.db.fetchall(query)
            product_ids = []
            for row in data:
                product_ids.append(row['product_id'])
            return product_ids
        return False

    def get_external_product_skus(self):
        if self.endpoint:
            query = """
                         SELECT sku from products where stock_endpoint_id = '{0}'
                         """.format(self.endpoint.endpoint_id)
            data = database.db.fetchall(query)
            product_ids = []
            for row in data:
                product_ids.append(row['sku'])
            return product_ids
        return False

    def get_external_product_ids(self, compsed=False, assembly=False):
        if self.endpoint:
            query = """
                         SELECT endpoint_product_id from products where stock_endpoint_id = {stock_endpoint_id} 
                         and composed = {composed} and assembly = {assembly}
                         """.format(stock_endpoint_id=self.endpoint.endpoint_id,
                                    composed=database.db.bool_to_db(compsed),
                                    assembly=database.db.bool_to_db(assembly))
            data = database.db.fetchall(query)
            product_ids = []
            for row in data:
                product_ids.append(row['endpoint_product_id'])
            return product_ids
        return False

    def import_products(self, category_id, channel):
        pass

    def import_stock(self):
        pass
