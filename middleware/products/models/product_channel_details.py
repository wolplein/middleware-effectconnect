from database import database
from .product_category import ProductCategory
from .product import Product
from .product_channel_details_images import ListOfProductChannelDetailsImages
from .product_channel_details_translations import ListOfProductChannelDetailsTranslations


class ProductChannelDetails:
    def __init__(self):
        self.id = 0
        self.channel_id = 0
        self.product_id = 0
        self.channel_product_identifier = 0
        self.price = 0.0
        self.category_id = 0
        self.category = False
        self.product_channel_details_images = []
        self.product_channel_details_translations = []
        self.product = False

    def not_exist(self):
        query = """
                SELECT id from product_channel_details where channel_product_identifier = '{0}'
                """.format(self.channel_product_identifier)
        data = database.db.fetchall(query)
        if len(data) == 1:
            self.id = data[0]['id']
            self.load_from_db()
            return False
        elif len(data) == 0:
            return True
        else:
            raise 'Duplicated key in product_channel_details {0}'.format(str(self.id))

    def load_from_db(self):
        data = database.db.fetchall('SELECT * FROM product_channel_details where id = {0}'.format(self.id))
        for row in data:
            self.channel_id = row['channel_id']
            self.product_id = row['product_id']
            self.channel_product_identifier = row['channel_product_identifier']
            self.price = row['price']
            self.category_id = row['category_id']

    def insert_product_channel_details(self):
        query = """
            INSERT INTO product_channel_details (channel_id, product_id, channel_product_identifier, price, category_id)
            VALUES ({channel_id}, {product_id}, '{channel_product_identifier}', {price}, {category_id})
        """
        query = query.format(channel_id=self.channel_id,
                             product_id=self.product_id,
                             channel_product_identifier=database.db.db_connection.escape_string(self.channel_product_identifier),
                             price=self.price,
                             category_id=self.category_id
                             )
        self.id = database.db.no_select_query(query, return_id=True)

    def update_price(self):
        query = """
            UPDATE product_channel_details SET price = {price} WHERE id = {id}
        """
        query = query.format(price=self.price,
                             id=self.id)
        database.db.no_select_query(query)


class ListOfProductChannelDetails:
    def __init__(self):
        self.list_of_pcds = []
        self.channel_id = False

    def import_prices(self, pricelist_id):
       pass

    def get_export_list(self):
        data = database.db.fetchall('SELECT * FROM product_channel_details')
        for row in data:
            product_channel_detail = ProductChannelDetails()
            product_channel_detail.id = row['id']
            product_channel_detail.channel_id = row['channel_id']
            product_channel_detail.product_id = row['product_id']
            product_channel_detail.channel_product_identifier = row['channel_product_identifier']
            product_channel_detail.price = row['price']

            product_channel_detail.category = ProductCategory()
            product_channel_detail.category.id = row['category_id']
            product_channel_detail.category.load_from_db()

            product_channel_detail.product = Product()
            product_channel_detail.product.product_id = row['product_id']
            product_channel_detail.product.load_from_db()

            list_of_product_channel_detail_images = ListOfProductChannelDetailsImages()
            list_of_product_channel_detail_images.load_list_of_product_channel_details_images(product_channel_detail.id)
            product_channel_detail.product_channel_details_images = list_of_product_channel_detail_images.list_of_pcdsi

            list_of_product_channel_detail_translations = ListOfProductChannelDetailsTranslations()
            list_of_product_channel_detail_translations.load_list_of_product_channel_details_translations(product_channel_detail.id)
            product_channel_detail.product_channel_details_translations = list_of_product_channel_detail_translations.list_of_pcdst

            self.list_of_pcds.append(product_channel_detail)

    def export_products(self):
        pass

    def get_list_per_channel(self):
        data = database.db.fetchall('SELECT * FROM product_channel_details where channel_id = {0}'.format(self.channel_id))
        for row in data:
            product_channel_detail = ProductChannelDetails()
            product_channel_detail.id = row['id']
            product_channel_detail.channel_id = row['channel_id']
            product_channel_detail.product_id = row['product_id']
            product_channel_detail.channel_product_identifier = row['channel_product_identifier']
            product_channel_detail.price = row['price']
            self.list_of_pcds.append(product_channel_detail)

    def update_products(self):
        pass