from database import database


class ProductCategory:
    def __init__(self):
        self.id = 0
        self.name = False
        self.channel_id = 0

    def insert_category(self):
        query = """
        INSERT INTO product_categories (id, name, channel_id) VALUES ('{id}', '{name}', {channel_id})
        """
        query = query.format(id=self.id,
                             name=database.db.db_connection.escape_string(self.name),
                             channel_id=self.channel_id)
        database.db.no_select_query(query)

    def not_exist(self):
        query = """
        SELECT id from product_categories where id = {0}
        """.format(self.id)
        data = database.db.fetchall(query)
        if len(data) == 1:
            return False
        elif len(data) == 0:
            return True
        else:
            raise 'Duplicated key in product_categories {0}'.format(str(self.id))

    def load_from_db(self):
        data = database.db.fetchall('SELECT * FROM product_categories where id = {0}'.format(self.id))
        for row in data:
            self.name = row['name']
            self.channel_id = row['channel_id']


class ProductCategories:
    def __init__(self):
        self.list_of_categories = []

    def importchannel_product_identifier_categories(self, channel):
        pass

    def load_list_of_categories_by_channel_id(self, channel_id):
        query = 'SELECT * FROM product_categories WHERE channel_id = {0}'.format(channel_id)
        data = database.db.fetchall(query)
        for row in data:
            product_category = ProductCategory()
            product_category.id = row['id']
            product_category.name = row['name']
            product_category.channel_id = row['channel_id']
            self.list_of_categories.append(product_category)
