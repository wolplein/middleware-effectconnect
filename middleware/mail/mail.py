import smtplib
import os
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
from email.mime.base import MIMEBase
from email import encoders
from config import config


class Mail:
    id = False
    name = False
    email = False
    report = False


def send_mail(email_send, subject, message=False, filename=False):
    msg = MIMEMultipart()
    msg['From'] = config.EMAIL_USER
    msg['To'] = email_send
    msg['Subject'] = subject

    if message:
        msg.attach(MIMEText(message, 'plain'))

    if filename:
        attachment = open(filename, 'rb')
        part = MIMEBase('application', 'octet-stream')
        part.set_payload(attachment.read())
        encoders.encode_base64(part)
        part.add_header('Content-Disposition', "attachment; filename= " + os.path.basename(filename))
        msg.attach(part)

    text = msg.as_string()
    server = smtplib.SMTP(config.EMAIL_SMTP, config.EMAIL_SMTP_PORT)
    server.starttls()
    server.login(config.EMAIL_USER, config.EMAIL_PASSWORD)

    server.sendmail(config.EMAIL_USER, email_send, text)
    server.quit()
