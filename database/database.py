import pymysql
import datetime
from config import db_config


class Database:
    def __init__(self):
        self.db_connection = False

    def db_connect(self):
        self.db_connection = pymysql.connect(db_config.MYSQL_DATABASE_HOST, db_config.MYSQL_DATABASE_USER,
                                             db_config.MYSQL_DATABASE_PASSWORD,
                                             db_config.MYSQL_DATABASE_DB,
                                             use_unicode=True,
                                             charset="utf8")

    @staticmethod
    def dict_factory(cursor, row):
        d = {}
        for idx, col in enumerate(cursor.description):
            d[col[0]] = row[idx]
        return d

    def get_cursor(self):
        self.db_connect()
        return self.db_connection.cursor()

    def no_select_query(self, query, return_id=False):
        self.db_connect()
        with self.db_connection as cursor:
            cursor.execute(query)
            self.db_connection.commit()
            if return_id:
                return cursor.lastrowid

    def fetchall(self, query):
        l = []
        self.db_connect()
        with self.db_connection as cursor:
            cursor.execute(query)
            data = cursor.fetchall()
            for row in data:
                d = {}
                for idx, col in enumerate(cursor.description):
                    d[col[0]] = row[idx]
                l.append(d)
            return l

    @staticmethod
    def datetime_to_sql(datetime_value):
        if datetime_value:
            return "'" + datetime_value.strftime("%Y-%m-%d %H:%M:%S") + "'"
        else:
            return "NULL"

    @staticmethod
    def sql_to_datetime(sql_val):
        if type(sql_val) is datetime.datetime:
            return sql_val
        try:
            return datetime.datetime.strptime(sql_val, "%Y-%m-%d %H:%M:%S")
        except:
            return False

    @staticmethod
    def int_or_null(int_or_null):
        if int_or_null:
            return int_or_null
        else:
            return "NULL"

    def string_or_null(self, string_or_null):
        if string_or_null:
            return "'" + self.db_connection.escape_string(string_or_null) + "'"
        else:
            return "NULL"

    @staticmethod
    def bool_to_db(bool_val):
        if bool_val:
            return 1
        else:
            return 0

    @staticmethod
    def db_to_bool(db_val):
        if db_val == 1:
            return True
        else:
            return False

    @staticmethod
    def list_to_in(int_list):
        return "(" + ",".join(map(str, int_list)) + ")"

    @staticmethod
    def int_list_to_comma_string(int_list):
        return ";".join(map(str, int_list))


db = Database()
